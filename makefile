
OPT    +=  -DHDF5 -DH5_USE_16_API
#OPT    +=  -DECC_NONZERO
OPT    +=  -DORBITCORRECTION   # this will correct the orbit if the halos already overlap
                               # the dark halos are assumed to follow a Hernquist profile in this

EXEC   = CombineGalaxies

OBJS   = main.o  globvars.o \
	 load.o save.o \
	 move.o turn.o \
	 nrsrc/nrutil.o



INCL   = globvars.h nrsrc/nrutil.h makefile


CC       =    icc        # sets the C-compiler (default)
OPTIMIZE =   -O3 -Wall    # optimization and warning flags (default)


LIBS   =    -lm  -lgsl -lgslcblas  -lhdf5

CFLAGS =    $(OPTIONS) $(OPT)  $(OPTIMIZE)  $(GSL_INCL)


$(EXEC): $(OBJS) 
	$(CC) $(OBJS) $(LIBS)  -o $(EXEC)  

$(OBJS): $(INCL) 

clean:
	 rm -f $(OBJS) $(EXEC)


