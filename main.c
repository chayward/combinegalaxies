/*** code to combine two galaxies such that they collide
     on a parabolic encounter ***/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "globvars.h"




int main(int argc, char *argv[])
{
  double theta1, phi1, theta2, phi2;
  double rmin, rstart, ecc=1.0, velfactor= 1.0;
  char gal_fname1[120], gal_fname2[120], gal_output[120];
  char   p_gal1[120], p_gal2[120], cmd[120];  /* , temp[120]; */
  FILE *fd;


#ifdef ECC_NONZERO
  if(argc != 11)
#else
  if(argc != 10)
#endif
    {
      printf("\nwrong number of arguments\n");
      printf
#ifdef ECC_NONZERO
	("call with:\n\n<fname_gal1> <theta1> <phi1>\n<fname_gal2> <theta2> <phi2>\n<rmin> <rstart>\n<eccentricity>\n<fname_galout>\n\n");
#else
	("call with:\n\n<fname_gal1> <theta1> <phi1>\n<fname_gal2> <theta2> <phi2>\n<rmin> <rstart>\n<fname_galout>\n\n");
#endif
      printf("(angles in degrees.)\n\n");
      exit(0);
    }

#ifdef ECC_NONZERO
#ifdef ORBITCORRECTION
  printf(" can't have ECC_NONZERO and ORBITCORRECTION turned on at the same time.\n");
  printf(" please fix.\n");
  exit(0);
#endif
#endif

  strcpy(gal_fname1, argv[1]);
  theta1 = atof(argv[2]);
  phi1 = atof(argv[3]);

  strcpy(gal_fname2, argv[4]);
  theta2 = atof(argv[5]);
  phi2 = atof(argv[6]);

  rmin = atof(argv[7]);
  rstart = atof(argv[8]);

#ifdef ECC_NONZERO
  ecc= atof(argv[9]);
  strcpy(gal_output, argv[10]);
#else
  strcpy(gal_output, argv[9]);
#endif



  /* Do we have the parameter files */
  /* ------------------------------ */
  strcpy(p_gal1,"");
/*   new method (not used): *.parameters (without hdf5 in there) */
/*
  if(strstr(gal_fname1,"hdf5"))
    strncpy(p_gal1, gal_fname1, strlen(gal_fname1)-5);
  else
*/
    strcpy(p_gal1, gal_fname1);    /* old method: *.dat.parameters */

  strcat(p_gal1, ".parameters");
  if((fd=fopen(p_gal1,"r")))
        fclose(fd);
  else
  {
        fprintf(stderr,"\nProblem: can't find 1st galaxy IC file '%s'.\n",p_gal1);
        exit(0);
  }
  
  strcpy(p_gal2,"");
/*
  strcpy(temp,"");
  strncpy(temp, gal_fname2, strlen(gal_fname2)-5);
  if(strstr(gal_fname1,"hdf5"))
    strcpy(p_gal2, temp);
  else
*/
    strcpy(p_gal2, gal_fname2);

  strcat(p_gal2, ".parameters");
  if((fd=fopen(p_gal2,"r")))
        fclose(fd);
  else
  {
        fprintf(stderr,"\nProblem: can't find 2nd galaxy IC file '%s'.\n",p_gal2);
        exit(0);
  }



  /* Now do the work */
  /* --------------- */
  load_particles(gal_fname1, &gal[0], &header[0]);
  load_particles(gal_fname2, &gal[1], &header[1]);

  turn_galaxy(&gal[0], theta1, phi1);
  turn_galaxy(&gal[1], theta2, phi2);


  move_galaxies(&gal[0], &gal[1], rmin, rstart, ecc);


  /* output file */
  if(!(strstr(gal_output,"hdf5")))
    strcat(gal_output, ".hdf5");

  save_combined(gal_output, &gal[0], &gal[1]);



  /* Write a parameter file */
  /* ---------------------- */
  strcat(gal_output,".parameters");
  if(!(fd=fopen(gal_output,"w")))
  {
        fprintf(stderr,"\nCan't find file '%s'.\n",gal_output);
        exit(0);
  }

  fprintf(fd,"theta1  \t%g\n",theta1);
  fprintf(fd,"phi1    \t%g\n",phi1);
  fprintf(fd,"theta2  \t%g\n",theta2);
  fprintf(fd,"phi2    \t%g\n",phi2);
  fprintf(fd,"rmin    \t%g\n",rmin);
  fprintf(fd,"rstart  \t%g\n",rstart);
  fprintf(fd,"ecc     \t%g\n",ecc);
  fprintf(fd,"velf    \t%g\n",velfactor);
  fclose(fd);

  strcpy(cmd, "");
  strcpy(cmd,"echo GAL1 >> ");
  strcat(cmd,gal_output);
  printf("cmd= %s\n",cmd);
  system(cmd);
  
  strcpy(cmd, "");
  strcpy(cmd,"head -n 35 ");
  strcat(cmd,p_gal1);
  strcat(cmd," >> ");
  strcat(cmd,gal_output);
  printf("cmd= %s\n",cmd);
  system(cmd);

  strcpy(cmd, "");
  strcpy(cmd,"echo GAL2 >> ");
  strcat(cmd,gal_output);
  printf("cmd= %s\n",cmd);
  system(cmd);

  strcpy(cmd, "");
  strcpy(cmd,"head -n 35 ");
  strcat(cmd,p_gal2);
  strcat(cmd," >> ");
  strcat(cmd,gal_output);
  printf("cmd= %s\n",cmd);
  system(cmd);



  return 0;
}
